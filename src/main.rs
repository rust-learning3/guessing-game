use std::io; //using the input/output library (io) that comes from the standard library (std)
use rand::Rng; 
use std::cmp::Ordering;

fn main() {
    println!("Guess the number!");
    
    // Generate rdm number
    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        // Receiving user input
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line"); 

        print!("You guessed {guess}");
        
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue
        };
    
        // Deciding if the result is good or not.
        match guess.cmp(&secret_number) {
            Ordering::Equal => {
                println!("You guessed the number!");
                break;
            },
            Ordering::Less => println!("Too little"),
            Ordering::Greater => println!("Too large")
        }
    }
    

}
